## BDD & ATDD Presentation
Presentation about what the BDD really is and how to guide the development with ATDD

### Installation

```sh
npm install & bower install
```

### Start server

```sh
gulp serve
```
